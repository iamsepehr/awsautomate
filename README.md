# awsautomate

automating Elastifile cloud file system with REST API

**configure.sh**: bash script to configure EMS server

**addCapacity.sh**: adding nodes

**queryEcfs.sh**: calculates cluster capacity and performance

**removeNode.sh**: remove nodes
